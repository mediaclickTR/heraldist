<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 21.03.2019
     * Time: 17:45
     */

    namespace Mediapress\Modules\Content\Observers;
    use Mediapress\Heraldist\Models\Form;
    use Illuminate\Support\Facades\Cache;

    class FormObserver
    {
        public function created(Form $form)
        {
            Cache::flush();
        }
        public function deleted(Form $form)
        {
            Cache::flush();
        }
    }
