<?php

namespace Mediapress\Contracts;

interface IMessagingModule extends IModule
{
    public function mailSender($data,$emails,$title,$subject,$fromEmail);

    public function mailSenderWithQueue($data,$emails,$title,$subject,$fromEmail,$queueTime=1);
}