<?php

namespace Mediapress\Heraldist\Traits;

use Collective\Html\FormFacade;
use Mediapress\Foundation\UserAgent\UserAgent;
use Mediapress\Heraldist\Mail\FormSender;
use Mediapress\Heraldist\Models\Message;
use Mail;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\Url;
use Illuminate\Support\Facades\Storage;

trait FormTrait {

    public function getFormVariables( $form )
    {
        $formVariables = [];

        $mediapress = mediapress();

        $hidden_field = '';
        $hidden_field .= "
        ". csrf_field();
        $hidden_field .= "
        ". FormFacade::hidden('_id', encrypt($form->id));
        $hidden_field .= "
        ". FormFacade::hidden('_trackid', '');
        if ($mediapress->visit) {
            $hidden_field .= "
        ". FormFacade::hidden('_visit_id', encrypt($mediapress->visit->id));
        }

        $hidden_field .= "
        " . FormFacade::hidden('_called_class', encrypt(get_called_class()));

        $scripts =
            '<script type="text/javascript">
    var _paq = window._paq || [];
    _paq.push([\'trackPageView\']);
    _paq.push([\'enableLinkTracking\']);
    (function() {
        var u=\'' . url('/') . '/\';
        _paq.push([\'setTrackerUrl\', u+\'matomo\']);
        _paq.push([\'setSiteId\', \'' . $mediapress->website->id . '\']);
        var d=document, g=d.createElement(\'script\'), s=d.getElementsByTagName(\'script\')[0];
        g.type=\'text/javascript\'; g.async=true; g.defer=true; g.src=u+\'vendor/mediapress/flow/flow.js\'; s.parentNode.insertBefore(g,s);
    })();
</script>
<script type="text/javascript" src="' . mp_asset("vendor/mediapress/js/intlTelInput.min.js") . '"></script><script>


    $.each($(".telmask"), function (key, value) {
        window.intlTelInput(value, {
            preferredCountries: ["' . mediapress()->activeLanguage->code . '"],
            separateDialCode: true,
            autoPlaceholder: "polite"
        });
        var obj = $(value);

        obj.on("focus", function() {
            var activePlaceholder = obj.attr("placeholder");
            var newMask = activePlaceholder.replace(/[0-9]/g, "0");
            obj.mask(newMask);
        })

        obj.on("countrychange", function (e, countryData) {
            obj.val("");
            obj.attr("maxlength", 20);
        });
    });

    function movePlus(){
        let objs = $(".telmask");

        $.each(objs, function (key, value) {
            obj = $(value);
            var flag = obj.closest(".iti--allow-dropdown").find("div.iti__selected-dial-code");
            if (obj.val().indexOf("+") != 0 || obj.val() == "") {
                obj.val(flag.text() + " " + obj.val());
            }
        });
    }


</script>
<script src="https://www.google.com/recaptcha/api.js?hl='. $mediapress->activeLanguage->code . '" async defer></script>
<script>
    $(\'form[action="' .  route("form.store") .'"]\').on("submit",function(){
        movePlus();
    });
</script>
';
        $formVariables["hidden"] = $hidden_field;
        $formVariables["scripts"] = $scripts;
        $formVariables["styles"] = '<link rel="stylesheet" href="' . mp_asset("vendor/mediapress/css/intlTelInput.min.css") . '" />';

        return $formVariables;
    }

    public function postForm( $form, $request )
    {
        dd(get_called_class() . " yolunda formPost() metodu bulunamadı!");
    }

    public function sendForm($form, $request, $extras = [])
    {
        $data = $this->collectData($form, $request);

        if (session('mediapressUtm')) {
            $data['utm'] = session('mediapressUtm');
        } else {
            $data['utm'] = null;
        }
        if (session('mediapressSource')) {
            $data['source'] = session('mediapressSource');
        } else {
            $data['source'] = null;
        }

        $data['track_id'] = $request->_trackid;
        $data['visit_id'] = $request->_visit_id ? decrypt($request->_visit_id) : null;

        $data['ip'] = $request->ip();

        $message = $this->saveForm($form, $data);

        if ($form->receiver) {
            $formClass = 'App\Mail\Form';
            Mail::to($form->receiver)->send(new $formClass($message, $form->name, $extras));
        }

        $referer = $this->clearReferer($request->header('referer'));

        if (Mail::failures()) {
            return redirect($referer . '?error=true')->withErrors(implode(', ', Mail::failures()));
        }
        $parseReferer = parse_url($referer);

        $referer = $parseReferer['scheme'] . '://' . $parseReferer['host'] . $parseReferer['path'];
        $success_message = ($form->success) ? $form->success : 'form.success.message';

        $language = $this->getLanguage($request, $referer);

        return redirect($referer . '?success=true')->with('status', langPart($success_message, $success_message,[],$language->id));
    }

    public function saveForm($form, $data)
    {
        $message = new Message();
        $message->data = $data['data'];
        $message->email = $data['email'];
        $message->form_id = $form->id;
        $message->name = $form->name;
        $message->agent = $this->userAgent();
        $message->track_id = $data['track_id'];
        $message->visit_id = $data['visit_id'];
        $message->source = $data['source'];
        $message->utm = $data['utm'];
        $message->ip = $data['ip'];
        $message->save();
        return $message;
    }

    private function userAgent()
    {
        $agent = new UserAgent();
        return [
            'platform' => $agent->platform(),
            'browser' => $agent->browser(),
            'device' => $agent->getDevice(),
            'version' => $agent->version($agent->browser()),
            'language' => $agent->languages(),
            'raw' => $agent->getUserAgent(),
            'headers' => $agent->getHttpHeaders(),
        ];
    }

    private function ip($request)
    {
        if ($request->server('HTTP_CF_CONNECTING_IP')) {
            return $request->server('HTTP_CF_CONNECTING_IP');
        }
        return $request->server('REMOTE_ADDR');


    }

    private function collectData($form, $request)
    {
        $list = [];
        //   unset($dataArray['g-recaptcha-response'], $dataArray['_token']);
        $dataArray = $request->all();

        foreach ($dataArray as $name => $value) {

            if( in_array($name, ["_token", "_id", "_trackid", "_called_class", "_visit_id", "g-recaptcha-response"]) ) {
                continue;
            }
            if (!is_array($value)){
                $key = is_file($value) ? $name : hash('ripemd128', $name);
            }
            else{
                $key =  hash('ripemd128', $name);
            }
            $value = $dataArray[$name];

            if (is_array($value)) {
                $value = implode(', ', $value);
            } elseif (is_file($value)) {
                $file = $request[$key];
                $file_name = $file->store(null, 'public');
                $value = "<a href='" . route("download.uploadedFile", ["url" => $file_name]) . "' target='_blank'>" . langPart("panel.uploaded.file", "Yüklenmiş Dosyayı Görüntüle") . "</a>";
            }

            $list[$key] = [
                "label" => langPart($name, $name, [], 760),
                'key' => $key,
                'type' => null,
                'value' => $value,
            ];
        }

        return ['data' => $list, 'email' => $form->receiver];
    }

    public function clearReferer($header)
    {
        $referer = parse_url($header);
        $url = $referer['scheme'] . '://' . $referer['host'] . $referer['path'];
        if (isset($referer['query'])) {


            $query = explode('&', str_replace('?', '&', $referer['query']));
            $queryString = [];
            foreach ($query as $q) {
                $q = explode('=', $q);
                $key = null;
                $value = null;
                if (isset($q[0])) {
                    $key = $q[0];
                }
                if (isset($q[1])) {
                    $value = $q[1];
                }
                if ($key != 'error' && $key != 'success') {
                    $queryString[$key] = $value;
                }
            }
            if (count($queryString)) {
                $url .= '?' . http_build_query($queryString);
            }
        }

        return $url;
    }

    public function getLanguage($request, $referer)
    {

        $parsedUrl = parse_url(preg_replace('/\?.*/', '', $referer));
        $language = null;
        if (isset($parsedUrl['path'])) {
            $url = Url::where('url', $parsedUrl['path'])->first();
            if ($url) {
                $relation = $url->relation;
                if ($relation) {
                    $language = $relation->language;
                }
            }

        }
        if (!$language) {
            $language = Language::find(616);
        }
        return $language;
    }
}
