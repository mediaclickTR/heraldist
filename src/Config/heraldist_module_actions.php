<?php
return [

    'forms' => [
        'name' => 'HeraldistPanel::auth.sections.forms',
        'item_model' => \Mediapress\Heraldist\Models\Form::class,
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'actions' => [
            'index' => [
                'name' => "MPCorePanel::auth.actions.list",
            ],
            'create' => [
                'name' => "MPCorePanel::auth.actions.create",
            ],
            'update' => [
                'name' => "MPCorePanel::auth.actions.update",
            ],
            'delete' => [
                'name' => "MPCorePanel::auth.actions.delete",
            ],
            'manage_components' => [
                'name' => "AuthPanel::auth.actions.manage_components",
            ],
        ],
        'subs' => [
            'detail' => [
                'name' => 'Form Detayı',
                'item_model' => \Mediapress\Heraldist\Models\FormDetail::class,
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => true,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    /*'edit_apply' =>  [
                    'name' => "Güncelleme Öner",
                    'variables' =>['languages']
                    ],*/
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ],
                    'reorder' => [
                        'name' => "MPCorePanel::auth.actions.reorder",
                    ],
                ]
            ]
        ]
    ],
    "messages" => [
        'name' => 'HeraldistPanel::auth.sections.messages',
        'item_model' => \Mediapress\Heraldist\Models\Message::class,
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'actions' => [
            'index' => [
                'name' => "MPCorePanel::auth.actions.list",
            ],
            'view' => [
                'name' => "MPCorePanel::auth.actions.view",
            ],
            'mark_all_unread' => [
                'name' => "Okunmadı Olarak İşaretle",
            ],
            'delete' => [
                'name' => "MPCorePanel::auth.actions.delete",
            ]
        ],
        'subs' => [
        ]
    ],

    "ebulletin" => [
        'name' => 'HeraldistPanel::auth.sections.ebulletin_lists',
        'item_model' => \Mediapress\Modules\Entity\Models\EBulletinUser::class,
        'item_id'=>"*",
        'actions' => [
            'index' => [
                'name' => "MPCorePanel::auth.actions.list",
                'variables' => []
            ],
            'update' => [
                'name' => "MPCorePanel::auth.actions.update"
            ],
            'delete' => [
                'name' => "MPCorePanel::auth.actions.delete",
            ],
            'export' => [
                'name' => "Excele Aktar",
                'variables' => ['file_type']
            ]
        ],
        'variations_title'=>"Sitelerin E-Bülten Listeleri",
        'variations'=>function ($root_item) {
            $websites = \Mediapress\Modules\Content\Models\Website::with('detail')->get();
            $sets = [];

            foreach ($websites as $website) {
                if (!$website->detail) {
                    continue;
                }
                $set = [];
                $set["name"] = 'HeraldistPanel::auth.sections.ebulletin_list';
                $set["name_affix"] = $website->name;
                $set['item_model'] = $root_item["item_model"];
                $set['item_id'] = $root_item["item_id"];
                $set['actions'] = $root_item["actions"];
                $set['settings'] = [
                    "auth_view_collapse" => "out"
                ];
                $set['data'] = [
                    "is_root" => false,
                    "is_variation" => true,
                    "is_sub" => false,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ];
                //$set['subs'] = $root_item['subs'];
                //data_set($set['subs'], '*.data.descendant_of_variation', true);

                $sets['website' . $website->id] = $set;
            }

            return $sets;
        }
    ],
];
