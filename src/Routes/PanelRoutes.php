<?php


const PREFIX_HERALDIST = 'prefix';
const INDEX_HERALDIST = 'index';
const ID_DELETE_HERALDIST = '{id}/delete';
const DELETE_HERALDIST = 'delete';
const CREATE_HERALDIST = '/create';
const CREATE2_HERALDIST = 'create';
const STORE2_HERALDIST = '/store';
const STORE_HERALDIST = 'store';
const UPDATE2_HERALDIST = '/update';
const UPDATE_HERALDIST = 'update';
const ID_EDIT_HERALDIST = '{id}/edit';
const ID_SHOW_HERALDIST = '{id}/show';
const SELECTED_REMOVE_HERALDIST = 'selectedRemove';

Route::group(['middleware' => 'panel.auth'], function () {
    Route::group([PREFIX_HERALDIST => 'Heraldist', 'as'=>'Heraldist.'], function () {

        /**
         * Base Modules
         * Include ebulletin, forms, messages
         */

        Route::group([PREFIX_HERALDIST => 'Ebulletins', 'as' => 'ebulletin.'], function () {
            Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'EBulletinManagerController@index']);
            Route::get('/list', ['as' => 'list.get', 'uses' => 'EBulletinManagerController@memberList']);
            Route::get('/excel/all', ['as' => 'excel.all', 'uses' => 'EBulletinManagerController@excelAll']);
            Route::get('/excel/new', ['as' => 'excel.new', 'uses' => 'EBulletinManagerController@excelNew']);
            Route::post('/list', ['as' => 'list.post', 'uses' => 'EBulletinManagerController@postUrlList']);
            Route::get('/ajax/{website_id?}', ['as' => 'ajax', 'uses' => 'EBulletinManagerController@ajax']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'EBulletinManagerController@formCreate']);
            Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'EBulletinManagerController@delete']);
            Route::post(SELECTED_REMOVE_HERALDIST, ['as' => SELECTED_REMOVE_HERALDIST, 'uses' => 'EBulletinManagerController@selectedRemove']);
            Route::get('getData', ['as' => 'getData', 'uses' => 'EBulletinManagerController@getData']);

        });

        Route::group([PREFIX_HERALDIST => 'Forms', 'as' => 'forms.'], function () {
            Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'FormController@index']);
            Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'FormController@create']);
            Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'FormController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'FormController@edit']);
            Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'FormController@update']);
            Route::get('/{id}/delete', ['as' => DELETE_HERALDIST, 'uses' => 'FormController@delete']);
            Route::get('/{id}/details', ['as' => 'details', 'uses' => 'FormController@details']);
            Route::get('/test', ['as' => 'test', 'uses' => 'FormController@test']);
            Route::post('/{id}/details/create', ['as' => 'detail.createOrUpdate', 'uses' => 'FormController@detailsCreateOrUpdate']);
        });

        Route::group([PREFIX_HERALDIST => 'Messages', 'as' => 'message.'], function () {
            Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'MessageController@index']);
            Route::get('show/{form_id}', ['as' => 'show', 'uses' => 'MessageController@show']);
            Route::get('detail/{id}', ['as' => 'detail', 'uses' => 'MessageController@detail']);
            Route::get('notRead/{id}', ['as' => 'notRead', 'uses' => 'MessageController@notRead']);
            Route::get('delete/{id}', ['as' => DELETE_HERALDIST, 'uses' => 'MessageController@delete']);
            Route::get('inbox/{id}', ['as' => 'inbox', 'uses' => 'MessageController@inbox']);
            Route::post('changeReadYes', ['as' => 'changeReadYes', 'uses' => 'MessageController@changeReadYes']);
            Route::get('changeReadNo/{id}', ['as' => 'changeReadNo', 'uses' => 'MessageController@changeReadNo']);
            Route::get('markUnread/{id}', ['as' => 'markUnread', 'uses' => 'MessageController@markUnread']);
            Route::get('downloadFile/{path}', ['as' => 'downloadFile', 'uses' => 'MessageController@downloadFile']);
            Route::post(SELECTED_REMOVE_HERALDIST, ['as' => SELECTED_REMOVE_HERALDIST, 'uses' => 'MessageController@selectedRemove']);
        });

        /**
         * Mailservice
         * Include MPMailler and Mailchimp mail service
         *
         */

        Route::group([PREFIX_HERALDIST => 'Mailservice'], function () {
            // Module Service choosing page
            Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\MailServiceController@index']);

            /**
             * MPMailler Mail Service
             */
            Route::group([PREFIX_HERALDIST => 'MPMailler'], function () {
                // Module Service processing page
                Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\MPmailler\MailServiceMPMaillerController@index']);

                Route::group([PREFIX_HERALDIST => 'Maillists', 'as' => 'mpmailler.maillists.'], function () {
                    Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\MPmailler\MaillistController@index']);
                    Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\MPmailler\MaillistController@create']);
                    Route::get(ID_EDIT_HERALDIST, ['as' => 'edit', 'uses' => 'Mailservice\MPmailler\MaillistController@edit']);
                    Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\MPmailler\MaillistController@store']);
                    Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\MPmailler\MaillistController@update']);
                    Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\MPmailler\MaillistController@delete']);
                    Route::get('{id}/mailSend', ['as' => 'mailsend', 'uses' => 'Mailservice\MPmailler\MaillistController@mailSend']);
                    Route::get('/{id}/mailSendSuccess', ['as' => 'mailsendsuccess', 'uses' => 'Mailservice\MPmailler\MaillistController@mailSendSuccess']);
                    Route::get('/{id}/sendedmails', ['as' => 'sendedmails', 'uses' => 'Mailservice\MPmailler\MaillistController@sendedMails']);
                    Route::get('/{id}/{maillist_id}/sendedmaildelete', ['as' => 'sendedmaildelete', 'uses' => 'Mailservice\MPmailler\MaillistController@sendedMailDelete']);
                });
                Route::group([PREFIX_HERALDIST => 'Mailtemplates', 'as' => 'mpmailler.mail_templates.'], function () {
                    Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\MPmailler\MailTemplateController@index']);
                    Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\MPmailler\MailTemplateController@create']);
                    Route::get(ID_EDIT_HERALDIST, ['as' => 'edit', 'uses' => 'Mailservice\MPmailler\MailTemplateController@edit']);
                    Route::get(ID_SHOW_HERALDIST, ['as' => 'show', 'uses' => 'Mailservice\MPmailler\MailTemplateController@show']);
                    Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\MPmailler\MailTemplateController@store']);
                    Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\MPmailler\MailTemplateController@update']);
                    Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\MPmailler\MailTemplateController@delete']);
                });
                Route::group([PREFIX_HERALDIST => 'EmailSender', 'as' => 'mpmailler.email_senders.'], function () {
                    Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\MPmailler\EmailSenderController@index']);
                    Route::get('/sendeds', ['as' => 'sendeds', 'uses' => 'Mailservice\MPmailler\EmailSenderController@sendeds']);
                    Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\MPmailler\EmailSenderController@create']);
                    Route::get(ID_EDIT_HERALDIST, ['as' => 'edit', 'uses' => 'Mailservice\MPmailler\EmailSenderController@edit']);
                    Route::get(ID_SHOW_HERALDIST, ['as' => 'show', 'uses' => 'Mailservice\MPmailler\EmailSenderController@show']);
                    Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\MPmailler\EmailSenderController@store']);
                    Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\MPmailler\EmailSenderController@update']);
                    Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\MPmailler\EmailSenderController@delete']);
                    Route::post(SELECTED_REMOVE_HERALDIST, ['as' => SELECTED_REMOVE_HERALDIST, 'uses' => 'Mailservice\MPmailler\EmailSenderController@selectedRemove']);
                });

                Route::group([PREFIX_HERALDIST => 'ContactManagement', 'as' => 'mpmailler.contact_management.'], function () {
                    Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\MPmailler\ContactManagementController@index']);
                    Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\MPmailler\ContactManagementController@create']);
                    Route::get(ID_EDIT_HERALDIST, ['as' => 'edit', 'uses' => 'Mailservice\MPmailler\ContactManagementController@edit']);
                    Route::get(ID_SHOW_HERALDIST, ['as' => 'show', 'uses' => 'Mailservice\MPmailler\ContactManagementController@show']);
                    Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\MPmailler\ContactManagementController@store']);
                    Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\MPmailler\ContactManagementController@update']);
                    Route::post('/send', ['as' => 'send', 'uses' => 'Mailservice\MPmailler\ContactManagementController@send']);
                    Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\MPmailler\ContactManagementController@delete']);
                    Route::post('export', ['as' => 'export', 'uses' => 'Mailservice\MPmailler\ContactManagementController@export']);
                    Route::post('/import', ['as' => 'import', 'uses' => 'Mailservice\MPmailler\ContactManagementController@import']);
                });
            });

            /**
             * Server (Mailchimp) Mail Service
             */

            Route::group([PREFIX_HERALDIST => 'Mailchimp'], function () {

                // Module Service processing page
                Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MailServiceMailchimpController@index']);

                Route::group([PREFIX_HERALDIST => 'Maillists', 'as' => 'mailchimp.maillists.'], function () {
                    Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MaillistController@index']);
                    Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MaillistController@create']);
                    Route::get(ID_EDIT_HERALDIST, ['as' => 'edit', 'uses' => 'Mailservice\Mailchimp\MaillistController@edit']);
                    Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MaillistController@store']);
                    Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MaillistController@update']);
                    Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MaillistController@delete']);
                });

                /**
                 * Maillist child
                 * Maillists/{id}/members/islem
                 */
                Route::group([PREFIX_HERALDIST => 'Maillists/{id}', 'as' => 'mailchimp.maillists.members.'], function () {
                    Route::get('/members', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MembersController@index']);
                    Route::get('/members/create', ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MembersController@create']);
                    Route::post('/members/store', ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MembersController@store']);
                    Route::get('/members/edit/{memberid}', ['as' => 'edit', 'uses' => 'Mailservice\Mailchimp\MembersController@edit']);
                    Route::post('/members/update/{memberid}', ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MembersController@update']);
                    Route::get('/members/delete/{memberid}', ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MembersController@delete']);
                });
                Route::group([PREFIX_HERALDIST => 'EmailSender', 'as' => 'mailchimp.emailsender.'], function () {
                    Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\Mailchimp\EmailSenderController@index']);
                    Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\Mailchimp\EmailSenderController@create']);
                    Route::get(ID_EDIT_HERALDIST, ['as' => 'edit', 'uses' => 'Mailservice\Mailchimp\EmailSenderController@edit']);
                    Route::get(ID_SHOW_HERALDIST, ['as' => 'show', 'uses' => 'Mailservice\Mailchimp\EmailSenderController@show']);
                    Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\EmailSenderController@store']);
                    Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\EmailSenderController@update']);
                    Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\EmailSenderController@delete']);
                    Route::get('/email/{id}/send', ['as' => 'send', 'uses' => 'Mailservice\Mailchimp\EmailSenderController@emailSend']);
                    Route::get('/emailAjaxList', ['as' => 'emailAjaxList', 'uses' => 'Mailservice\Mailchimp\EmailSenderController@emailAjaxList']);
                });
                Route::group([PREFIX_HERALDIST => 'Mailtemplates', 'as' => 'mailchimp.mail_templates.'], function () {
                    Route::get('/', ['as' => INDEX_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MailTemplateController@index']);
                    Route::get(CREATE_HERALDIST, ['as' => CREATE2_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MailTemplateController@create']);
                    Route::get(ID_EDIT_HERALDIST, ['as' => 'edit', 'uses' => 'Mailservice\Mailchimp\MailTemplateController@edit']);
                    Route::get(ID_SHOW_HERALDIST, ['as' => 'show', 'uses' => 'Mailservice\Mailchimp\MailTemplateController@show']);
                    Route::post(STORE2_HERALDIST, ['as' => STORE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MailTemplateController@store']);
                    Route::post(UPDATE2_HERALDIST, ['as' => UPDATE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MailTemplateController@update']);
                    Route::get(ID_DELETE_HERALDIST, ['as' => DELETE_HERALDIST, 'uses' => 'Mailservice\Mailchimp\MailTemplateController@delete']);
                });
            });
        });
    });
});
