<?php namespace Mediapress\Heraldist\DataTable;

use Mediapress\Modules\Content\Models\Cluster;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\View;

trait TableBuilderTrait
{
    /**
     * @param Builder $builder
     * @param Cluster|null $cluster
     * @return mixed
     */
    public function columns(Builder $builder, Cluster $cluster = null)
    {
        $prefix = '';
        if($this instanceof CategoryController){
           $prefix = 'Category';
        }
        if($cluster){
            $consoleLog = 'console.log("DataTable Path: \\\\App\\\\DataTable\\\\' . $prefix.$cluster->clusterType->name .'")';
            View::share("dataTablePathConsole", $consoleLog);
        }
        if ($cluster && method_exists('\App\DataTable\\' .$prefix. $cluster->clusterType->name, 'columns')) {
            return app('\App\DataTable\\' . $prefix.$cluster->clusterType->name)->columns($builder);
        }
        return app('Mediapress\Heraldist\DataTable\Tables\\' . class_basename(__CLASS__))->columns($builder);
    }

    /**
     * @param Builder $builder
     * @param Cluster|null $cluster
     * @return string
     */
    public function dataTableName(Builder $builder, Cluster $cluster = null)
    {
        $prefix = '';
        if($this instanceof CategoryController){
            $prefix = 'Category';
        }
        return '\App\DataTable\\' .$prefix. $cluster->clusterType->name;
    }


    /**
     * @param null $cluster_id
     * @param null $parameter
     * @return mixed
     */
    public function ajax($website_id,$cluster_id = null, $parameter = null)
    {
        $prefix = '';
        if($this instanceof CategoryController){
            $prefix = 'Category';
        }
        if ($cluster_id)
        {
            $cluster = Cluster::with("clusterType")->findOrFail($cluster_id);
            if (method_exists('\App\DataTable\\' . $prefix.$cluster->clusterType->name, 'ajax')) {
                return app('\App\DataTable\\' .$prefix. $cluster->clusterType->name)->ajax($website_id,$cluster, $parameter);
            }
            if ($cluster){
                return app('Mediapress\Heraldist\DataTable\Tables\\' . class_basename(__CLASS__))->ajax($website_id,$cluster, $parameter);
            }
        }
        return app('Mediapress\Heraldist\DataTable\Tables\\' . class_basename(__CLASS__))->ajax($website_id,$cluster_id, $parameter);
    }
}
