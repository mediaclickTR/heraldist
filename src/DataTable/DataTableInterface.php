<?php

namespace Mediapress\Heraldist\DataTable;

use Yajra\Datatables\Html\Builder;

interface DataTableInterface
{
    public function columns(Builder $builder);
}
