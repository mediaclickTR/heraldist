<?php

namespace Mediapress\Heraldist;

use Mediapress\Contracts\HeraldistInterface;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Heraldist\Jobs\SendEmailJob;
use Mediapress\Heraldist\Jobs\SendMailable;
use Illuminate\Support\Facades\Mail;
use Mediapress\Models\MPModule;
use Illuminate\Routing\Router;
use Mediapress\Heraldist\Models\Ebulletin;
use Mediapress\Heraldist\Models\Form;
use Mediapress\Heraldist\Models\MailTemplate;

class Heraldist extends MPModule //implements HeraldistInterface
{
    public const SUBMENU = "submenu";
    public const TYPE = "type";
    public const TITLE = "title";
    public const URL = "url";
    public $name = "Heraldist";
    public $url = "mp-admin/Heraldist";

    public function fillMenu($menu)
    {
        #region Header Menu > Datasets > Forms > Set

        // Tab Name Set
        data_set($menu, 'header_menu.datasets.cols.forms.name',trans("HeraldistPanel::menu_titles.forms"));

        $forms = $this->getForms();

        foreach ($forms as $form){
            $datasets_cols_forms_rows_set[] = [
                    self::TYPE => self::SUBMENU,
                    self::TITLE => '<i class="fab fa-wpforms"></i> '.panelLangPart($form->name,$form->name),
                    self::URL => route("Heraldist.message.show", $form->id)
            ];
        }
        $datasets_cols_forms_rows_set[] = [
            self::TYPE => self::SUBMENU,
            self::TITLE =>trans("HeraldistPanel::menu_titles.ebulletin"),
            self::URL => route("Heraldist.ebulletin.index")
        ];
        $datasets_cols_forms_rows_set[] = [
                self::TYPE => self::SUBMENU,
                self::TITLE =>trans("HeraldistPanel::menu_titles.forms.manage"),
                self::URL => route("Heraldist.forms.index")
        ];

        return dataGetAndMerge($menu, 'header_menu.datasets.cols.forms.rows',$datasets_cols_forms_rows_set);
        #endregion

    }

    public function getForms()
    {
        /** @var Website $website */
        $website = session("panel.website");
        $website_id = $website->id;

        return Form::whereHas('websites', function($query) use ($website_id) {
            $query->where('id', $website_id);
        })->get();

    }

    public function getForm($id){
        return Form::where("id",$id)->first();
    }

    public function getEbulletins()
    {
        /** @var Website $website */
        $website = session("panel.website");
        $website_id = $website->id;

        return Ebulletin::where("website_id", $website_id)->get();
    }

    public function getEmailTemplates()
    {
        return MailTemplate::get();
    }

    /**
     * @param array $data
     * @param string $emails, $title, $subject, $fromEmail
     * @return void
     */
    public function mailSender($data,$emails,$subject,$fromEmail)
    {
        try{
            Mail::send("HeraldistPanel::mail_service.mpmailler.mail_templates.template", $data, function ($message) use ($emails,$subject,$fromEmail) {
                $message->to($emails)->subject($subject);
                $message->from($fromEmail, $subject);
            });
        }
        catch(\Exception $e){
            dd("İşlem sırasında hata oluştu", $e);
        }
    }

    /**
     * @param array $data
     * @param string $emails, $title, $subject, $fromEmail
     * @param integer $queueTime
     * @return void
     */
    public function mailSenderWithQueue($data,$emails,$title,$subject,$fromEmail,$queueTime=1)
    {
        try{
            dispatch(new SendEmailJob($emails,$data,$subject,$fromEmail, $title))->delay(Carbon::now()->addSeconds($queueTime));
        }
        catch(\Exception $e){
            dd("İşlem sırasında hata oluştu", $e);
        }
    }

}
