<?php

return [
    'messages' => 'Mesajlar',
    'forms' => 'Formlar',
    'new_total' => 'Yeni/Toplam',
    'gomessages' => 'Mesajlara Git',
    'open-new-page' => 'Yeni sayfada aç',
    'check-unread' => 'Okunmadı olarak işaretle',
    'form_messages' => ':form adlı Forma Ait Mesajlar',
    'empty_message_box'=>'Mesaj kutunuz boş.',
    'open.new.page' => 'Yeni sayfada aç'
];