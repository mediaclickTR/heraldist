<?php

return [
    'forms' => 'Formlar',
    'form_create' => 'Form Oluştur',
    'form_edit' => 'Form Düzenle',
    'success_message' => 'Başarı Mesajı',
    'error_message' => 'Hata Mesajı',
    'buton_name' => 'Buton Adı',
    'receiver_email' => 'Alıcı E-Posta (Boş Bırakılabilir)',
    'recaptcha_key' => 'Google ReCaptcha Key',
    'recaptcha_secret' => 'Google ReCaptcha Secret',
    /*
     * Forms/details
     */
    'form_detail' => 'Form Detay',
    'inbox' => 'Gelen Kutusu',
    'form.inputs' => 'Form Elemanlarını Yönet',
    'lang.part.save.info'=>'Dikkat kaydetme işlemi otomatik yapılır. İlgili alanları doldurmanız yeterlidir.',
    'notes'=>'Notlar'
];