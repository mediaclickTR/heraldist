<?php

return [
    'ebulletins'=>'E-Bülten Kayıtları',
    'excelall' => 'Tüm üyeleri excel\'e aktar',
    'name'=> 'Ad Soyad',
    'email'=> 'E-Posta',
    'phone'=> 'Telefon',
    'subscription.date'=>'Abonelik Tarihi'
];
