@csrf
@if(isset($emailsender))
    <input type="hidden" value="{!! $emailsender->id !!}" name="id">
@endif
    <div class="form-group rel focus">
        <div class="tit">{!! trans("HeraldistPanel::mail_service.emailsender_name") !!}</div>
        <input type="text" name="title" value="{{ old('subject_line',  isset($emailsender->settings->title) ? $emailsender->settings->title : null) }}">
    </div>

    <div class="form-group" id="list">
        <div class="tit">{!! trans("HeraldistPanel::mail_service.select_maillist") !!}</div>
        <select name="maillist_id" id="listoptions" required="required">
            <option value="">{!! trans("MPCorePanel::general.selection") !!}</option>
            @foreach($maillist_all as $row)
                <option @if(isset($emailsender)) @if($row->id == $emailsender->recipients->list_id) selected @endif @endif value="{!! $row->id !!}">{!! $row->name !!}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group" id="list">
        <div class="tit">{!! trans("HeraldistPanel::mail_service.select_template") !!}</div>
        <select name="template_id" id="listoptions" required="required">
            <option value="">{!! trans("MPCorePanel::general.selection") !!}</option>
            @foreach($mailtemplates  as $template)
                    <option @if(isset($emailsender)) @if($template->id == $emailsender->settings->template_id) selected @endif @endif value="{!! $template->id !!}">{!! $template->name !!}</option>
            @endforeach
        </select>
    </div>

    <div class="float-right">
        <button class="btn btn-primary">{!! trans("MPCorePanel::general.send") !!}</button>
    </div>

@push("styles")
    <style>
        select{
            width:100%;
        }
    </style>
@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#pre-selected-options').multiSelect({
                selectableHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.eligible") !!}</div>",
                selectionHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.selected") !!}</div>",
            });

            // Eğer yeni mail listesi oluşturmak isterse
            $('#quick-list').click(function(e){
                $('#list').hide();
                $('#quick-list').hide();
                $('#new-maillist').show();
                $('#select-inlist').show();
                $("#listoptions").prop("selectedIndex", -1);
            });
            $('#select-inlist').click(function(e){
                $('#list').show();
                $('#quick-list').show();
                $('#new-maillist').hide();
                $('#select-inlist').hide();
            });
        });
    </script>
@endpush