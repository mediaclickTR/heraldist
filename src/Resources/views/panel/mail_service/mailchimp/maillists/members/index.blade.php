@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
            <div class="float-left">
                <h1 class="title">"{!! $maillist->name !!}" {!! trans('HeraldistPanel::mail_service.lists_user') !!} ({!! $total !!})</h1>
                <a href="{!! route('Heraldist.mailchimp.maillists.index') !!}"> <i class="fa fa-arrow-circle-left"></i> {!! trans('HeraldistPanel::mail_service.back_maillist') !!}</a>
            </div>
            @if(count($members)<=0)
                <div class="alert alert-warning">{!! trans('MPCorePanel::general.nodata') !!}</div>
                <div class="float-right">
                    <a href="{!! route('Heraldist.mailchimp.maillists.members.create',$maillistid) !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans('MPCorePanel::general.new_add') !!}</a>
                </div>
            @else
            <div class="float-right">
                <a href="{!! route('Heraldist.mailchimp.maillists.members.create',$maillistid) !!}" class="btn btn-primary"> <i class="fa fa-plus"></i> {!! trans('MPCorePanel::general.new_add') !!}</a>
            </div>
            <table>
                <thead>
                <tr>
                    <th>{!! trans('MPCorePanel::general.id') !!}</th>
                    <th>{!! trans('MPCorePanel::general.email') !!}</th>
                    <th>{!! trans('MPCorePanel::general.actions') !!}</th>
                </tr>
                </thead>
                @foreach($members as $member)
                <tr>
                    <td>{!! $member->id !!}</td>
                    <td>{!! $member->email_address !!}</td>
                    <td>
                        <select onchange="locationOnChange(this.value);">
                            <option value="#">{!! trans('MPCorePanel::general.selection') !!}</option>
                            <option value="{!! route('Heraldist.mailchimp.maillists.members.edit',['id'=>$maillistid,'memberid'=>$member->id]) !!}">{!! trans('MPCorePanel::general.edit') !!}</option>
                            <option value="{!! route('Heraldist.mailchimp.maillists.members.delete',['id'=>$maillistid,'memberid'=>$member->id]) !!}">{!! trans('MPCorePanel::general.delete') !!}</option>
                        </select>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        @endif
@endsection
