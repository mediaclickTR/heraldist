@csrf
@if(isset($member))
    <input type="hidden" name="maillistid" value="{!! $maillistid !!}">
    <input type="hidden" name="memberid" value="{!! $member->id !!}">
    <div class="form-group rel focus">
        <label>{!! trans("MPCorePanel::general.email") !!}</label>
        <input type="text" name="email_address" value="{{ old('email_address',  isset($member->email_address) ? $member->email_address : null) }}">
    </div>
    @else
    <div class="form-group rel focus">
        <div class="tit">{!! trans("HeraldistPanel::mail_service.email_selection") !!}</div>
        <select id='pre-selected-options' class="form-control" name="emails[]" multiple='multiple'>
            @foreach($data['table_names'] as $id => $row)
                @foreach($data['rows'] as $key=>$value)
                    <optgroup label="{!! $row !!}">
                        @if($row==$value['name'])
                            <option @if(isset($maillist)) @if(in_array($value['email'],$emails)) selected @endif @endif value="{!! $value['email'] !!}">{!! $value['email'] !!}</option>
                        @endif
                    </optgroup>
                @endforeach
            @endforeach
        </select>
    </div>
@endif
<div class="float-right">
    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>

@push('scripts')
    <script type="application/javascript">
        $(document).ready(function () {
            CKEDITOR.replace( 'editor1' );
        });
        $('#pre-selected-options').multiSelect({
            selectableHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.eligible") !!}</div>",
            selectionHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.selected") !!}</div>",
        });
    </script>
@endpush