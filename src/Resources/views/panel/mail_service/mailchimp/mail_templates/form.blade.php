@include("MPCorePanel::inc.errors")
@csrf
@if(isset($mailtemplate))
    <input type="hidden" value="{!! $mailtemplate->id !!}" name="id">
@endif
@csrf
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.template_name") !!}</label>
    <input type="text" name="name" value="{{ old('name',  isset($mailtemplate->name) ? $mailtemplate->name : null) }}">
</div>
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.template_detail") !!}</label><br/>
    <textarea id="editor1" name="html">{{ isset($mailtemplate->html) ? $mailtemplate->html : null }}</textarea>
</div>