@csrf
@if(isset($emailsenders))
    <input type="hidden" value="{!! $emailsenders->id !!}" name="id">
@endif

<div class="tab-content">
    <div class="tab-pane" id="tab1">
        <div class="form-group" id="list">
            <div class="tit">{!! trans("HeraldistPanel::mail_service.select_maillist") !!}</div>
            <select name="maillist_id" id="listoptions">
                <option value="">{!! trans("MPCorePanel::general.selection") !!}</option>
                @foreach($maillist_all as $row)
                    <option @if(isset($emailsenders)) @if($row->id == $emailsenders->maillist_id) selected @endif @endif value="{!! $row->id !!}">{!! $row->name !!}</option>
                @endforeach
            </select>
        </div>
        <a href="#" id="select-inlist" style="display:none;" class="tablink"><i class="fa fa-chevron-left"></i> {!! trans("MPCorePanel::general.list_selection") !!}</a>
        <a href="#" id="quick-list" class="tablink"><i class="fa fa-chevron-right"></i> {!! trans("HeraldistPanel::mail_service.create_quicklist") !!}</a><br/><br/>
        <div id="new-maillist" style="display:none;">
            <div class="tit" for="">{!! trans("HeraldistPanel::mail_service.create_maillist") !!}</div>
            <div class="form-group rel focus">
                <select id='pre-selected-options' class="form-control" name="emails[]" multiple='multiple'>
                    @foreach($data['table_names'] as $id => $row)
                        @foreach($data['rows'] as $key=>$value)
                            <optgroup label="{!! $row !!}">
                                @if($row==$value['name'])
                                    <option @if(isset($maillist)) @if(in_array($value['email'],$emails)) selected @endif @endif value="{!! $value['email'] !!}-{!! $value['username'] !!}">{!! $value['email'] !!}</option>
                                @endif
                            </optgroup>
                         @endforeach
                    @endforeach
                </select>
            </div>
        </div>
        <ul class="pager wizard">
            <li class="next float-right"><a href="javascript:void();" class="btn btn-primary">{!! trans("MPCorePanel::general.next") !!} <i class="fa fa-angle-double-right"></i></a></li>
        </ul>
    </div>

    <div class="tab-pane" id="tab2">
            <div class="form-group" id="template">
                <div class="tit">{!! trans("HeraldistPanel::mail_service.select_template") !!}</div>
                <select name="mailtemplate_id" id="templateoptions" required="">
                    <option value="">{!! trans("MPCorePanel::general.selection") !!}</option>
                    @foreach($mailtemplates as $template)
                        <option @if(isset($emailsenders)) @if($template->id == $emailsenders->maillist_id) selected @endif @endif value="{!! $template->id !!}">{!! $template->name !!}</option>
                    @endforeach
                </select>
             </div>
            <a href="#" id="select-intemplate" style="display:none;" class="tablink"><i class="fa fa-chevron-left"></i> {!! trans("MPCorePanel::general.selection") !!}</a>
            <a href="#" id="quick-template" class="tablink"><i class="fa fa-chevron-right"></i> {!! trans("HeraldistPanel::mail_service.create_quicktemplate") !!}</a><br/><br/>
            <div id="new-template" style="display:none;">
                @include("HeraldistPanel::mail_service.mpmailler.mail_templates.form")
            </div>
            <div class="float-right">
                <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
            </div>
            <ul class="pager wizard">
                <li class="previous float-right"><a style="color:#f3dd24 !important;font-weight:bold !important;" href="javascript:;"><i class="fa fa-angle-double-left"></i>Geri Dön</a></li>
            </ul>
        </div>
</div>

@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
@endpush

@push('styles')
    <style>
        select{
            width: 100% !important;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#pre-selected-options').multiSelect({
                selectableHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.eligible") !!}</div>",
                selectionHeader: "<div class='custom-header'>{!! trans("MPCorePanel::general.selected") !!}</div>",
            });

            // Eğer yeni mail listesi oluşturmak isterse
            $('#quick-list').click(function(e){
                e.preventDefault();
                $('#list').hide();
                $('#quick-list').hide();
                $('#new-maillist').show();
                $('#select-inlist').show();
                $("#listoptions").prop("selectedIndex", -1);
            });
            $('#select-inlist').click(function(e){
                e.preventDefault();
                $('#list').show();
                $('#quick-list').show();
                $('#new-maillist').hide();
                $('#select-inlist').hide();
            });
            // Eğer yeni mail şablonu oluşturmak isterse
            $('#quick-template').click(function(e){
                e.preventDefault();
                $("#templateoptions").prop("selectedIndex", -1);
                $('#template').hide();
                $('#quick-template').hide();
                $('#new-template').show();
                $('#select-intemplate').show();
            });
            $('#select-intemplate').click(function(e){
                e.preventDefault();
                $('#template').show();
                $('#quick-template').show();
                $('#new-template').hide();
                $('#select-intemplate').hide();
            });

            $('.rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;
                    var $percent = ($current/$total) * 100;
                    $('.rootwizard').find('.progress-bar').css({width:$percent+'%'});
                }});
        });
    </script>
@endpush