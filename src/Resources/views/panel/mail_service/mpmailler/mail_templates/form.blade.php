@include("MPCorePanel::inc.errors")
@csrf
@if(isset($mailtemplate))
    <input type="hidden" value="{!! $mailtemplate->id !!}" name="id">
@endif

@push('specific.styles')
    <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}" />
@endpush

<div class="form-group rel">
    <label>{!! trans("EntityPanel::entitylist.select_websites") !!}</label><br/><br/>
    {!!Form::select('website_id[]', $websites,isset($mailtemplate) ? $mailtemplate->websites->pluck("id") : session()->get('panel.website')->id, ['class' => 'multiple','multiple', 'required'])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.template_name") !!}</label>
    <input type="text" name="name" value="{{ old('name',  isset($mailtemplate->name) ? $mailtemplate->name : null) }}">
</div>
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.template_title") !!}</label>
    <input type="text" name="mail_title" value="{{ old('name',  isset($mailtemplate->mail_title) ? $mailtemplate->mail_title : null) }}">
</div>
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.template_subject") !!}</label>
    <input type="text" name="mail_subject" value="{{ old('name',  isset($mailtemplate->mail_subject) ? $mailtemplate->mail_subject : null) }}">
</div>
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.template_content") !!}</label><br/>
    <textarea id="editor1" name="mail_detail">{{ isset($mailtemplate->mail_detail) ? $mailtemplate->mail_detail : null }}</textarea>
</div>
<div class="form-group rel focus">
    <label>{!! trans("HeraldistPanel::mail_service.template_detail") !!}</label><br/>
    <textarea style="min-height:500px;" name="body" id="editor2">{{ isset($mailtemplate->body) ? $mailtemplate->body : null }}</textarea>
    <code>{!! trans("HeraldistPanel::mail_service.template_create_info") !!}</code>
</div>

@push("specific.scripts")
    <script src="{!! asset('vendor/mediapress/js/jquery.bootstrap.wizard.min.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/fm.selectator.jquery.js') !!}"></script>
    <script src="{!! asset('vendor/mediapress/js/plugins-cke/ckeditor/ckeditor.js') !!}"></script>
@endpush

@push("scripts")
    <script>
        $(document).ready(function() {
            $('.multiple').selectator({
                showAllOptionsOnFocus: true,
                searchFields: 'value text subtitle right'
            });
            $(document).ready(function () {
                CKEDITOR.replace( 'editor1' );
            });
            $(document).ready(function () {
                CKEDITOR.replace( 'editor2' );
            });
        });
    </script>


@endpush