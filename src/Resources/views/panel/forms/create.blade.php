@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("HeraldistPanel::forms.form_create") !!}</div>
        @include('HeraldistPanel::forms.form', ['submitButtonText' => trans("MPCorePanel::general.save")])
    </div>
@endsection