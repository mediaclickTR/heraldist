@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("HeraldistPanel::forms.form_edit") !!}</div>
        <div class="col-md-12">
            @include('HeraldistPanel::forms.form', ['submitButtonText' => trans("MPCorePanel::general.edit")])
        </div>
    </div>
@endsection