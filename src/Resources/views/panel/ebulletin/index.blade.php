@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("HeraldistPanel::ebulletin.ebulletins") !!}</div>
        <div class="float-left">
            <select class="nice" id="multiple-processings" onchange="locationOnChange(this.value);">
                <option value="">{!! trans('MPCorePanel::general.multiple_transactions') !!}</option>
                <option  {!! (request()->has('list') && request('list') == 'all') ? "selected" : '' !!} value="?list=all">{!! trans('MPCorePanel::general.all_website_records') !!}</option>
            </select>
            @if(request()->get('list') && request()->get('list')=="all")
                <a href="{!! route("Heraldist.ebulletin.index") !!}" class="filter-block">
                    <i class="fa fa-remove"></i> {!! trans('MPCorePanel::general.clear_filters') !!}
                </a>
            @endif
        </div>
        @if(userAction('ebulletin.export',true,false))
            <div class="float-right">
                <a class="btn btn-primary" href="{!! route('Heraldist.ebulletin.excel.all') !!}"> <i class="fa fa-file-excel"></i> {!! trans("HeraldistPanel::ebulletin.excelall") !!}</a>
            </div>
        @endif
        <div class="clearfix"></div>
        <div class="table-field">
            {!! $dataTable->table() !!}
        </div>
    </div>
@endsection
@push("scripts")
    <script>
        $(document).ready(function(){
            $('#multiple-processings').change(function(){
                $("#selectedRemove").submit();
            });
        });
    </script>
@endpush
