@extends('MPCorePanel::inc.app')
@section('content')

    @push('specific.styles')
        <link rel="stylesheet" href="{!! asset('vendor/mediapress/css/selector.css') !!}"/>
    @endpush

    <div class="container-fluid">
        @include('MPCorePanel::inc.breadcrumb')
        <div class="page">
            <div class="float-left">
                <div class="title">{!! trans("HeraldistPanel::messages.form_messages",['form'=>$form]) !!}</div>
            </div>
            <div class="clearfix"></div>
            @if(count($messages)>0)
                <div class="float-right">
                    <a class="btn btn-primary" id="remove-buton" href="{{ route('panel.logout') }}"
                       onclick="event.preventDefault();
                                document.getElementById('form').submit();">
                        <i class="fa fa-trash"></i> {!! trans("MPCorePanel::general.selected-delete") !!}
                    </a>
                </div>
                <div class="clearfix"></div>
                <article>
                    <div class="message-box">
                        <form id="form" action="{!! route('Heraldist.message.selectedRemove') !!}" method="POST">
                            @csrf
                            <div class="item">
                                <ul class="hierarchical_slide hierarchical_slide_inView">
                                    @foreach($messages as $message)
                                        <li id="mesaj{!! $message->id !!}" data-id="{!! $message->id !!}"
                                            class="{{$message->read==0?"unread":"read"}}">
                                            <div class="row">
                                                <div class="col-1">
                                                    <div class="checkbox">
                                                        <label for="male">
                                                            <input type="checkbox" name="checkbox[]"
                                                                   value="{!! $message->id !!}" id="checkbox">
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="head">
                                                        <span>{!! strtoupper(substr($message->email,0,2)) !!}</span>
                                                        {!! $message->email !!}
                                                    </div>
                                                </div>
                                                <div class="col-5">
                                                    <div

                                                            class="head">{!! str_limit($message->first,70) !!}</div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="float-right">
                                                        <select class="float-right" onchange="locationOnChange(this.value);">
                                                            <option
                                                                    value="#">{!! trans("MPCorePanel::general.selection") !!}</option>
                                                            <option
                                                                    value="{!! route('Heraldist.message.markUnread',$message->id) !!}">{!! trans("HeraldistPanel::messages.check-unread") !!}</option>
                                                        </select>
                                                        <i>{!! translateDate("d/m/y H:i",$message->created_at) !!}</i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="detail-content">
                                                <table>

                                                    <tr>
                                                        <td>{!! trans("MPCorePanel::general.reads") !!}</td>
                                                        <td>@if($message->read==1) {!! trans("MPCorePanel::general.read") !!} @else {!! trans("MPCorePanel::general.unread") !!} @endif</td>
                                                    </tr>
                                                    @if(isset($message->data))

                                                        @foreach($message->data as $key=>$data)
                                                            @if($data['value'] == $message->email)
                                                                <tr>
                                                                    <td>{!! trans("MPCorePanel::general.email") !!}</td>
                                                                    <td>
                                                                        <a href="mailto:{!! $message->email !!}">&lt;{{$message->email}}
                                                                            &gt;</a></td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{ $data['label'] }}</td>
                                                                    <td>
                                                                        @if(isset($data['attr_type']) && $data['attr_type'] == "file")
                                                                            <a href="{!! route('Heraldist.message.downloadFile', encrypt($data['value'])) !!}">Dosyayı İndir</a>
                                                                        @else
                                                                            {{$data['value']}}
                                                                            @if(is_url($data['value']))
                                                                                <br/><a href="{{$data['value']}}" class="btn"
                                                                                        target="_blank">{!! trans("HeraldistPanel::messages.open.new.page") !!}</a>
                                                                            @endif
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </table>
                                                <table>
                                                    <tbody><tr>
                                                        <td><img src="{!! asset('vendor/mediapress/images/date.png') !!}" alt="">Form Doldurma Tarihi</td>
                                                        <td>{!! \Carbon\Carbon::parse($message->created_at)->formatLocalized('%d %B %Y') !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><img src="{!! asset('vendor/mediapress/images/clock.png') !!}" alt="">Doldurulma Saati</td>
                                                        <td>{!! \Carbon\Carbon::parse($message->created_at)->format('H.i') !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><img src="{!! asset('vendor/mediapress/images/ip.png') !!}" alt="">IP</td>
                                                        <td>{!! $message->ip !!}</td>
                                                    </tr>
                                                    @if($message->source)
                                                        <tr>
                                                            <td><img src="{!! asset('vendor/mediapress/images/chrome.png') !!}" alt="">Nereden Geldi</td>
                                                            <td><a href="{!! $message->source !!}" target="_blank">{!! $message->source !!}</a></td>
                                                        </tr>
                                                    @endif
                                                    <tr>
                                                        <td><img src="{!! asset('vendor/mediapress/images/chrome.png') !!}" alt="">OS / Browser</td>
                                                        <td>{!! $message->agent['platform'] !!} / {!! $message->agent['browser'] !!} / {!! $message->agent['version'] !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><img src="{!! asset('vendor/mediapress/images/desc.png') !!}" alt="">Gezdiği Sayfalar</td>
                                                        <td>
                                                            {!! arrayToLi($message->lastVisit) !!}
                                                        </td>
                                                    </tr>
                                                    @if($message->utm)
                                                        <tr>
                                                            <td>Utm Bilgileri</td>
                                                            <td>
                                                                {!! arrayToLi($message->utm,' = ') !!}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @if($message->id)
                                                        <tr>
                                                            <td>Form Doldurma</td>
                                                            <td>
                                                                <a class="btn-btn-sm" target="_blank" href="{!! route('admin.flows.show',['id'=>$message->id]) !!}">Kaydı İzle</a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    </tbody></table>
                                                <br/><br/>
                                                <div class="reply">
                                                    <a href="{!! route('Heraldist.message.markUnread',$message->id) !!}">{!! trans("HeraldistPanel::messages.check-unread") !!}</a>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </form>
                    </div>

                    {!! $messages->links() !!}

                    @else
                        <div
                                class="alert alert-warning">{!! trans("HeraldistPanel::messages.empty_message_box") !!}</div>
                    @endif
                </article>
        </div>
    </div>
@endsection

@push("styles")
    <style>
        .nice-select span:after {
            margin-top: -1px !important;
        }
    </style>
@endpush

@push("scripts")
    <script>
        $(window).scroll(function () {
            if (($(window).scrollTop()) > $(".message-box .list .item:nth-child(3)").offset()['top'] - 500) {
                $('.message-box .list .item:nth-child(3)> ul').addClass('hierarchical_slide_inView');
            }
        });
        // Okundu olarak işaretle
        $(document).ready(function () {
            $("[id*=mesaj].unread").click(function () {
                $(this).addClass("read");
                $(this).removeClass("unread");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('Heraldist.message.changeReadYes') }}",
                    type: "post",
                    data: id = {'id': $(this).attr("data-id")},
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            });
            if (window.location.hash != "") {
                $(window.location.hash).trigger("click");
                $(window.location.hash + " .head").trigger("click");
            }
        });

    </script>
@endpush
