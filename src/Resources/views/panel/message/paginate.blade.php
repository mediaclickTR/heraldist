@if ($paginator->hasPages())
    <div class="btn-group">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a href="javascript:void(0);" class="btn disabled">
                <i class="fa fa-angle-left"></i>
            </a>
        @else

            <a href="{{ $paginator->previousPageUrl() }}" class="btn">
                <i class="fa fa-angle-left"></i>
            </a>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="btn">
                <i class="fa fa-angle-right"></i>
            </a>
        @else
            <a href="javascript:void(0);" class="btn disabled">
                <i class="fa fa-angle-right"></i>
            </a>
        @endif
    </div>
@endif
