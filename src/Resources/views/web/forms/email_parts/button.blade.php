@if(isset($field))
    <div class="form-group">
        @include('HeraldistWeb::forms.email_parts.label',['field'=>$field])
        <{!! $field['tag'] !!} {!! $field->attributes(['class'=>'form-control']) !!} id
        ="{!! $field['id'] !!}" >
        {!! $field->options[0]['label'] !!}
        @include('HeraldistWeb::forms.email_main',['fields'=>$field->children])
    </{!! $field['tag'] !!}>
    </div>
@endif