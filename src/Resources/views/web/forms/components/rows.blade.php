@if(isset($field))
    <div class="mc-row {!! $field['className'][0] =="f-row"||$field['className'][0] =="" ? "col-md-12" : $field['className'][0] !!}" id="{!! $field['id'] !!}" >
        @include('HeraldistWeb::forms.main',['fields'=>$field->children])
    </div>
@endif
