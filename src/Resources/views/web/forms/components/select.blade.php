@if(isset($field))
    <div class="form-group">
        @include('HeraldistWeb::forms.components.label',['field'=>$field])
        <{!! $field['tag'] !!} {!! $field->attributes() !!} id="{!! $field['id'] !!}" >
        {!! $field['content'] !!}
        @foreach($field['options'] as $option)
            <option value="{!! $option['value'] !!}" {!! $option['selected'] ? 'selected="selected"' :'' !!}>{!! $option['label'] !!}</option>
        @endforeach
    </{!! $field['tag'] !!}>
    </div>
@endif