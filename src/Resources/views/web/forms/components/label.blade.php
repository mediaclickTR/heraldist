@if(isset($field))
    @if(isset($field->config['label']))
        <label for="{!! $field['id'] !!}">{!! $field->config['label'] !!}</label>
    @endif
@endif