@if(isset($field))
    <div class="{!! @implode(' ',$field['className']) !!} mc-col" id="{!! $field['id'] !!}" style="width: {!! $field['config']['width'] !!}" >
        @include('HeraldistWeb::forms.main',['fields'=>$field->children])
    </div>
@endif
