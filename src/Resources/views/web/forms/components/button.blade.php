@if(isset($field))
    <div class="form-group">
        <{!! $field['tag'] !!}  {!! $field->attributes() !!} id
        ="{!! $field['id'] !!}" >
        {!! $field->options[0]['label'] !!}
        @include('HeraldistWeb::forms.main',['fields'=>$field->children])
    </{!! $field['tag'] !!}>
    </div>
@endif