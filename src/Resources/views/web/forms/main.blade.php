@if(isset($fields))
    @foreach($fields as $field)
        @include('HeraldistWeb::forms.components.'.$field['key'],['field'=>new \Mediapress\Heraldist\Foundation\Field($field)])
    @endforeach
@endif
