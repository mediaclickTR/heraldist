<?php

namespace Mediapress\Heraldist\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Mediapress\Mail\EbulletinSender;
use Illuminate\Support\Facades\Mail;
use Mediapress\Heraldist\Models\Ebulletin;
use Mediapress\Modules\MPCore\Models\Extra;
use Mediapress\Http\Controllers\PanelController as Controller;

class WebEbulletinController extends Controller
{
    public const EMAIL = 'email';
    public const WEBSITE_ID = 'website_id';
    public const FILLED = 'filled';
    public const HERALDIST_PANEL_EBULLETIN_EMAIL = "HeraldistPanel::ebulletin.email";
    public const PHONE = 'phone';

    public function ebulletinSender(Request $request)
    {
        $this->validation($request);

        $ebulletin = new Ebulletin();

        $name = ucfirst($request->{'name'});
        $email = $request->{self::EMAIL};
        $phone = $request->{self::PHONE};
        $website_id = $request->{self::WEBSITE_ID};

        $ebulletin->name = ($name) ? $name : null;
        $ebulletin->email = ($email) ? $email : null;
        $ebulletin->phone = ($phone) ? $phone : null;
        $ebulletin->website_id = ($website_id) ? $website_id : 1;

        if ($ebulletin->save()) {

            if ($request->extras) {
                $extraList = [];
                foreach ($request->extras as $key => $extras) {
                    foreach ($extras as $extra) {
                        $extraList[] = Extra::insert(['key' => $key, 'value' => $extra]);
                    }
                }
                $ebulletin->extras()->saveMany($extraList);
            }

            if (config('mediapress.mail.sender_function') && function_exists('MAIL_SENDER_FUNCTION')) {
                MAIL_SENDER_FUNCTION('e-bulletin', $ebulletin);
            } else {
                Mail::to($ebulletin->email)->send(new EbulletinSender($ebulletin));
            }

            return response()->json(['response' => 'e-Bültene kayıt oldunuz. Lütfen e-posta kutunuzu kontrol ediniz.']);
        } else {
            return response()->json(['response' => 'e-Bültene kayıt sırasında hata oluştu. Lütfen daha sonra tekrar deneyiniz.'])->setStatusCode(422);
        }
    }

    public function validation(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'min:3',
                self::EMAIL => Rule::unique('e_bulletins')->whereNull('deleted_at')
            ],
            [
                'name.min' => trans("MPCorePanel::validation.min.numeric",
                    [self::FILLED, trans("HeraldistPanel::ebulletin.name")]),
                'email.required' => trans("MPCorePanel::validation.filled",
                    [self::FILLED, trans(self::HERALDIST_PANEL_EBULLETIN_EMAIL)]),
                'email.unique' => trans("MPCorePanel::validation.unique",
                    [self::FILLED, trans(self::HERALDIST_PANEL_EBULLETIN_EMAIL)]),
            ],
            [
                'name' => trans('HeraldistPanel::ebulletin.name'),
                self::EMAIL => trans(self::HERALDIST_PANEL_EBULLETIN_EMAIL),
                self::PHONE => trans('HeraldistPanel::ebulletin.phone')
            ]);
    }

    public function eBulletinActivation($id)
    {
        $ebulletin = Ebulletin::find(decrypt($id));

        if ($ebulletin->activated == 1) {
            throw new Exception("Hata oluştu", json_encode($ebulletin));
        }

        $ebulletin->activated = 1;
        $ebulletin->save();

        return view('vendor.mail.ebulletin_signed', compact('ebulletin'));
    }
}
