<?php

namespace Mediapress\Heraldist\Http\Controllers\Mailservice\Mailchimp;

use Illuminate\Http\Request;
use Mediapress\Heraldist\Models\MailTemplate;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Heraldist\Models\MailchimpTemplate;
use Mediapress\Http\Controllers\PanelController as Controller;

class MailTemplateController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const MESSAGE_MAIL_TEMPLATES_CREATE = 'message.mail_templates.create';
    public const TEMPLATES = "templates/";
    public const HERALDIST_MAILCHIMP_MAIL_TEMPLATES_INDEX = 'Heraldist.mailchimp.mail_templates.index';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    protected $apiKey;
    protected $apiUrl;
    protected $dataCenter;

    public function __construct()
    {
        $mailChimpApiKey = MPCore::settingSunFindValue('heraldist.mailchimp.apikey');
        // API KEY CHECK
        if ($mailChimpApiKey != '') {
            $this->apiKey = $mailChimpApiKey;
        } else {
            // dd("Mailchimp api key bulunamadı.4");
            return;
        }

        // APi URL
        $this->dataCenter = substr($this->apiKey, strpos($this->apiKey, '-') + 1);
        $this->apiUrl = "https://".$this->dataCenter.".api.mailchimp.com/3.0/";
    }

    public function index()
    {
        if (!userAction('message.email_sender.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $templates = curlGenerator($this->apiUrl."templates", $this->apiKey, "GET")->templates;
        $dataCenter = $this->dataCenter;
        return view('HeraldistPanel::mail_service.mailchimp.mail_templates.index', compact('templates', 'dataCenter'));
    }

    public function create()
    {
        if (!userAction(self::MESSAGE_MAIL_TEMPLATES_CREATE, true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        return view('HeraldistPanel::mail_service.mailchimp.mail_templates.create');
    }

    public function store(Request $request)
    {
        if (!userAction(self::MESSAGE_MAIL_TEMPLATES_CREATE, true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $fields = [
            'name' => 'Ad Soyad',
            'html' => 'Şablon Detayı',
        ];

        $rules = [
            'name' => 'required|max:100|regex:/^[A-Za-z\şŞıİçÇöÖüÜĞğ\s]+$/',
            'html' => 'required',
        ];

        $messages = [
            'name.required' => 'E-Posta Liste adı boş bırakılmamalıdır.',
            'html.required' => 'Şablon detayı boş bırakılmamalıdır.',
        ];

        $this->validate($request, $rules, $messages, $fields);

        $data = [
            'name' => $request->name,
            'html' => $request->html,
        ];

        $json = json_encode($data);

        curlGenerator($this->apiUrl.self::TEMPLATES, $this->apiKey, null, $json, "POST");

        return redirect(route(self::HERALDIST_MAILCHIMP_MAIL_TEMPLATES_INDEX))->with(self::MESSAGE,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function edit($id)
    {
        if (!userAction(self::MESSAGE_MAIL_TEMPLATES_CREATE, true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $mailtemplate = curlGenerator($this->apiUrl.self::TEMPLATES.$id, $this->apiKey, "GET");
        return view('HeraldistPanel::mail_service.mailchimp.mail_templates.edit', compact('mailtemplate'));
    }

    public function update(Request $request)
    {
        $json = json_encode([
            'name' => $request->name,
            'html' => $request->html
        ]);
        curlGenerator($this->apiUrl.self::TEMPLATES.$request->id, $this->apiKey, "PATCH", $json);
        return redirect(route(self::HERALDIST_MAILCHIMP_MAIL_TEMPLATES_INDEX))->with(self::MESSAGE,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function show($id)
    {
        $form = MailTemplate::find($id);
        return view('HeraldistPanel::mail_service.mailchimp.mail_templates.show', compact('form'));
    }

    public function delete($id)
    {
        if (!userAction('message.mail_templates.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        curlGenerator($this->apiUrl.self::TEMPLATES.$id, $this->apiKey, "DELETE");

        return redirect(route(self::HERALDIST_MAILCHIMP_MAIL_TEMPLATES_INDEX))->with(self::MESSAGE,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

}
