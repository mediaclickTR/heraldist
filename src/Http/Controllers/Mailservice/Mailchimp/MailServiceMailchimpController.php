<?php

namespace Mediapress\Heraldist\Http\Controllers\Mailservice\Mailchimp;

use Mediapress\Http\Controllers\PanelController as Controller;

class MailServiceMailchimpController extends Controller
{
    public function index()
    {
        return view('HeraldistPanel::mail_service.mailchimp.processing');
    }
}
