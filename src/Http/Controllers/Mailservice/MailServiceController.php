<?php

namespace Mediapress\Heraldist\Http\Controllers\Mailservice;

use Mediapress\Http\Controllers\PanelController as Controller;

class MailServiceController extends Controller
{
    public function index()
    {
        return view('HeraldistPanel::mail_service.processing');
    }
}
