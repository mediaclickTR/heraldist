<?php

namespace Mediapress\Heraldist\Http\Controllers\Mailservice\MPMailler;

use Mediapress\Http\Controllers\PanelController as Controller;

class MailServiceMPMaillerController extends Controller
{
    public function index()
    {
        return view('HeraldistPanel::mail_service.mpmailler.processing');
    }
}
