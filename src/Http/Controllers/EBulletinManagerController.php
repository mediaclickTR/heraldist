<?php

namespace Mediapress\Heraldist\Http\Controllers;

use Html;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use Maatwebsite\Excel\Facades\Excel;
use Mediapress\Heraldist\Models\Ebulletin;
use Mediapress\Heraldist\DataTable\TableBuilderTrait;
use Mediapress\Heraldist\Exports\EbulletinExport;
use Mediapress\Http\Controllers\PanelController as Controller;

class EBulletinManagerController extends Controller
{
    use TableBuilderTrait;

    public const CREATED_AT = "created_at";

    public function index(Builder $builder)
    {
        if (!userAction('ebulletin.index', true, false)) {
            return redirect()->to(url(route('accessdenied')));
        }

        if (request()->get('list') && request()->get('list') == "all") {
            $dataTable = $this->columns($builder)->ajax(route('Heraldist.ebulletin.ajax', null));
        } else {
            $dataTable = $this->columns($builder)->ajax(route('Heraldist.ebulletin.ajax',
                session("panel.website")->id));
        }

        return view('HeraldistPanel::ebulletin.index', compact('dataTable'));
    }

    public function excelAll()
    {
        if (!userAction('ebulletin.export', true, false)) {
            return redirect()->to(url(route('accessdenied')));
        }

        $date = Carbon::parse(date("d-m-Y"))->formatLocalized("%Y-%m-%d");

        $website_id = (session("panel.website")) ? session("panel.website")->id : 1;
        $data = Ebulletin::where("website_id", $website_id)->get(['id', 'name', "email", self::CREATED_AT]);

        return Excel::download(new EbulletinExport($data),
            trans("HeraldistPanel::ebulletin.ebulletins").' '.$date.'.xlsx');
    }

    public function excelNew()
    {
        $date = str_replace("/", "-",
            isset($_GET["date"]) ? (!empty($_GET["date"]) ? $_GET["date"] : "01/01/2018") : "01/01/2017");

        $website_id = (session("panel.website")) ? session("panel.website")->id : 1;
        $data = Ebulletin::where("website_id", $website_id)
            ->where(self::CREATED_AT, ">=", Carbon::parse($date)->formatLocalized("%Y-%m-%d"))
            ->orderBy(self::CREATED_AT)
            ->select("id", "name", "email", self::CREATED_AT)
            ->get();

        return Excel::download(new EbulletinExport($data),
            trans("HeraldistPanel::ebulletin.ebulletins").' '.$date.'.xlsx');
    }

    public function delete($id)
    {
        $ebulletin = Ebulletin::findOrFail($id);

        if ($ebulletin) {
            $ebulletin->delete();
        }

        return redirect()->back()->with('message', trans('MPCorePanel::general.success_message'));
    }

    public function selectedRemove(Request $request)
    {
        if ($request->checked) {
            foreach ($request->checked as $check) {
                Ebulletin::destroy($check);
            }
        } else {
            return redirect()->back()->withErrors([trans('MPCorePanel::general.danger_message')]);
        }

        return redirect()->back()->with('message', trans('MPCorePanel::general.success_message'));
    }

}
