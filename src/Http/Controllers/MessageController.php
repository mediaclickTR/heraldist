<?php

namespace Mediapress\Heraldist\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Mediapress\Heraldist\Models\Message;
use Illuminate\Support\Facades\DB;
use Mediapress\Heraldist\Models\Form;
use Mediapress\Http\Controllers\PanelController as Controller;

class MessageController extends Controller
{

    public const MESSAGE = 'message';
    public const SUBJECT = 'subject';
    public const VARNAME = self::MESSAGE;
    public const ACCESSDENIED = 'accessdenied';
    public const HERALDIST_MESSAGE_SHOW = 'Heraldist.message.show';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {
        if (!userAction('message.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $subjects = Message::groupBy(self::SUBJECT)->get();
        $counts = [];
        $totals = [];
        foreach ($subjects as $subject) {
            $count = Message::where(self::SUBJECT, $subject->subject)->where('deleted', 0)->where('read', '0')->count();
            $total = Message::where(self::SUBJECT, $subject->subject)->where('deleted', 0)->count();
            $counts[$subject->subject] = $count;
            $totals[$subject->subject] = $total;
        }

        return view('HeraldistPanel::message.index', compact('subjects', 'counts', 'totals'));
    }

    public function show($form_id, Request $request)
    {
        if (!userAction('message.show', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $messages = Message::where('form_id', $form_id)->orderBy("created_at", "DESC")->paginate(15);
        $form = Form::findOrFail($form_id)['name'];
        return view('HeraldistPanel::message.show', compact('messages', 'form'));
    }

    public function detail($id)
    {
        if (!userAction('message.views', true, false)) {
            $act = false;
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $message = Message::find($id);
        $message->read = 1;
        $message->save();

        return view('HeraldistPanel::message.detail', compact(self::VARNAME, 'act'));
    }

    public function notRead($id)
    {
        if (!userAction('message.mark_all_unread', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $message = Message::find($id);
        $message->read = 0;
        $message->save();

        return redirect(route(self::HERALDIST_MESSAGE_SHOW, [self::SUBJECT => $message->subject]))->with(self::VARNAME,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if (!userAction('message.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $message = Message::find($id);
        $message->deleted = 1;
        $message->save();

        return redirect(route(self::HERALDIST_MESSAGE_SHOW, [self::SUBJECT => $message->subject]))->with(self::VARNAME,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function inbox($id)
    {
        if (!userAction('message.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $message = Message::find($id);
        $message->deleted = 0;
        $message->save();
        return redirect(route(self::HERALDIST_MESSAGE_SHOW, [self::SUBJECT => $message->subject]))->with(self::VARNAME,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function changeReadYes(Request $request)
    {
        $message = Message::find($request->id);
        $message->read = 1;
        $message->save();
    }

    public function changeReadNo(Request $request)
    {
        $message = Message::find($request->id);
        $message->read = 0;
        $message->save();
    }

    public function markUnread($id)
    {
        $message = Message::find($id);
        $message->read = 0;
        $message->save();
        return redirect()->back()->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function selectedRemove(Request $request)
    {
        if ($request->checkbox) {
            foreach ($request->checkbox as $check) {
                $message = Message::find($check);
                if ($message) {
                    $form_id = $message->form_id;
                    $message->delete();
                }
            }
        }

        if (isset($form_id)) {
            return redirect(route(self::HERALDIST_MESSAGE_SHOW, ['form_id' => $form_id]))->with(self::VARNAME,
                trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::VARNAME, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function downloadFile($path)
    {
        if (auth()->check()) {
            $path = decrypt($path);
            $path = str_replace("/storage/", "", $path);
            return response()->download(storage_path("app/public/".$path));
        }
        return redirect()->route("login");
    }

}
