<?php

namespace Mediapress\Heraldist\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mediapress\Heraldist\Facades\Heraldist;
use Mediapress\Heraldist\Models\Form;
use Mediapress\Heraldist\Models\FormDetail;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Mediapress\Modules\MPCore\Facades\FilterEngine;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Http\Controllers\PanelController as Controller;

class FormController extends Controller
{
    public const ACCESSDENIED = 'accessdenied';
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';

    public function index()
    {
        if (!userAction('message.forms.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $forms = Form::where('id', '<>', 0);
        $queries = FilterEngine::filter($forms)['queries'];
        $forms = FilterEngine::filter($forms)['model'];
        return view('HeraldistPanel::forms.index', compact('forms', 'queries'));
    }

    public function create()
    {
        if (!userAction('message.forms.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view('HeraldistPanel::forms.create', compact('websites'));
    }

    public function edit($id)
    {
        $form = Form::find($id);
        $websites = MPCore::getInternalWebsites()->pluck("slug", "id");
        return view('HeraldistPanel::forms.edit', compact('form', 'id', 'websites'));
    }

    public function store(Request $request)
    {
        if (!userAction('message.forms.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $data = request()->except('_token', 'website_id');

        $insert = Form::firstOrCreate($data);

        if ($insert) {
            foreach ($request->website_id as $website) {
                $insert->websites()->attach([$website]);
            }
            UserActionLog::create(__CLASS__."@".__FUNCTION__, $insert);
        }

        return redirect(route("Heraldist.forms.index"))->with(self::MESSAGE,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $data = request()->except('_token', 'website_id');
        $update = Form::updateOrCreate(['id' => $id], $data);

        if ($update) {
            $update->websites()->detach();
            foreach ($request->website_id as $website) {
                $update->websites()->attach([$website]);
            }
            UserActionLog::update(__CLASS__."@".__FUNCTION__, $update);
        }

        return redirect(route('Heraldist.forms.index'))->with(self::MESSAGE,
            trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    public function delete($id)
    {
        if (!userAction('message.forms.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        $form = Form::find($id);

        if ($form) {
            $form->websites()->detach();
            $form->delete();
            $form_details = FormDetail::where("form_id", $id)->get();
            $ids = [];
            foreach ($form_details as $form_detail) {
                $ids[] = $form_detail->id;
            }
            FormDetail::whereIn('id', $ids)->delete();
            UserActionLog::delete(__CLASS__."@".__FUNCTION__, $form);
        }

        return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
    }

    #region Details

    public function details($form_id)
    {
        $form = Form::where("id", $form_id)->first();
        return view('HeraldistPanel::forms.details.details', compact('form_id', 'form'));
    }

    public function detailsCreateOrUpdate(Request $request, $form_id)
    {
        if (!userAction('message.forms.detail.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $update = Form::where("id", $form_id)->update(['formbuilder_json' => $request->form_data]);
        if ($update) {
            $model = Form::find($form_id);
            UserActionLog::update(__CLASS__."@".__FUNCTION__, $model);
            return redirect()->back()->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        } else {
            return redirect()->back()->with('error', trans('MPCorePanel::general.danger_message'));
        }
    }

    #endregion
}
