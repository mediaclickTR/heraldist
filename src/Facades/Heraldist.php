<?php

namespace Mediapress\Heraldist\Facades;

use Illuminate\Support\Facades\Facade;

class Heraldist extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'Heraldist';
    }

}
